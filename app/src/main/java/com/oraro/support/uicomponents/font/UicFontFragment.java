package com.oraro.support.uicomponents.font;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.oraro.support.R;
import com.oraro.uicomponents.font.FontUtils;

/**
 * Created by alexdeaconu on 20/08/16.
 */
public class UicFontFragment extends Fragment {

    private static final String TAG = UicFontFragment.class.getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_uic, container, false);
        Button btn = (Button) view.findViewById(R.id.button);

        long startTs = System.currentTimeMillis();
        FontUtils
                .loadFontFromAssets("fonts/minecraft.ttf")
                .recursive(true)
                .into(view)
                .loadFontFromAssets("android_7.ttf")
                .into(btn)
                .commit();
        Log.d(TAG, "Time took setting fonts: " + (System.currentTimeMillis() - startTs) + "ms");

        return view;
    }
}
