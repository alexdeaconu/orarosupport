package com.oraro.support.uicomponents.view.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.oraro.support.R;
import com.oraro.support.uicomponents.font.UicFontFragment;
import com.oraro.uicomponents.view.list.GenericListAdapter;
import com.oraro.uicomponents.view.list.SelectableItem;
import com.oraro.uicomponents.view.list.ViewHolder;
import com.oraro.uicomponents.view.list.annotation.ViewClickListener;
import com.oraro.uicomponents.view.list.annotation.ViewLayout;
import com.oraro.uicomponents.view.list.annotation.ViewText;

import java.util.ArrayList;
import java.util.List;

/**
 * Sample showing how to use the apis from the {@code com.oraro.uicomponents.view.list} package
 * <p/>
 * Created by alexdeaconu on 19/08/16.
 */
public class UicViewListFragment extends Fragment {

    private static final String TAG_FONT = "font_sample";

    private List<Object> featureList;
    private GenericListAdapter<Object> mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_uic_view_list, container, false);

        generateList();

        Button leftBtn = (Button) view.findViewById(R.id.add_100_left_btn);
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInterfaceModels(100);
            }
        });

        Button rightBtn = (Button) view.findViewById(R.id.add_100_right_btn);
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAnnotationModels(100);
            }
        });

        ListView viewList = (ListView) view.findViewById(R.id.listView);
        mAdapter = new GenericListAdapter<>(view.getContext(), featureList);
        viewList.setAdapter(mAdapter);

        return view;
    }

    private void generateList() {
        featureList = new ArrayList<>();
        featureList.add(new FeatureSelectable(R.layout.layout_uic_view_list_item_interface, "Font", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Item Font clicked", Toast.LENGTH_SHORT).show();
                FragmentActivity activity = getActivity();

                if (activity != null) {
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content, new UicFontFragment(), TAG_FONT)
                            .addToBackStack(TAG_FONT)
                            .commit();
                }
            }
        }));
        featureList.add(new Feature(R.layout.layout_uic_view_list_item_annotation, "Views", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Item Views clicked", Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private void addAnnotationModels(int n) {
        for (int i = 0; i < n; i++) {
            featureList.add(new Feature(R.layout.layout_uic_view_list_item_annotation, "Annotation " + i, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Item clicked ", Toast.LENGTH_SHORT).show();
                }
            }));
        }
        mAdapter.notifyDataSetChanged();
    }

    private void addInterfaceModels(int n) {
        for (int i = 0; i < n; i++) {
            featureList.add(new FeatureSelectable(R.layout.layout_uic_view_list_item_interface, "Interface " + i, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = getActivity();

                    if (activity != null) {
                        activity.getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.content, new UicFontFragment(), TAG_FONT)
                                .addToBackStack(TAG_FONT)
                                .commit();
                    }
                }
            }));
        }
        mAdapter.notifyDataSetChanged();
    }


    /**
     * Sample using view annotations
     */
    static class Feature {

        @ViewText(id = R.id.name)
        String mName;

        @ViewLayout
        int mLayout;

        @ViewClickListener
        View.OnClickListener mListener;

        Feature(int layout, String name, View.OnClickListener listener) {
            mLayout = layout;
            mName = name;
            mListener = listener;
        }
    }


    /**
     * Sample using the {@link SelectableItem}
     */
    static class FeatureSelectable implements SelectableItem {

        private final int mLayout;
        private final String mName;
        private final View.OnClickListener mListener;

        FeatureSelectable(int layout, String name, View.OnClickListener listener) {
            mLayout = layout;
            mName = name;
            mListener = listener;
        }

        @Override
        public void populateView(View convertView) {
            // setting the name to a text field
            TextView textView = ViewHolder.get(convertView, R.id.name);
            textView.setText(mName);

            // setting the listener on the view
            convertView.setOnClickListener(mListener);
        }

        @Override
        public int getLayoutRes(Context context) {
            return mLayout;
        }
    }
}
