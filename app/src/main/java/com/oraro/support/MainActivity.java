package com.oraro.support;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.oraro.support.uicomponents.view.list.UicViewListFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_UIC = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, new UicViewListFragment(), TAG_UIC).commit();
        }
    }
}
