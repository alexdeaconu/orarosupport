# README #

Collection of Android libraries containing helper classes for easing a developer's work.

## Modules ##
* UiComponents


### What is 'UiComponents' good for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


#### How do I get set up in my app? ####

*Gradle*

Add the following line inside *build.gradle*:
```
#!groovy

compile: 'com.oraro.support:ui-components:0.9.1'
```

*Maven*

Add the following line inside *pom.xml*:
```
#!maven

<dependency>
  <groupId>com.oraro.support</groupId>
  <artifactId>ui-components</artifactId>
  <version>0.9.1</version>
</dependency>
``` 

***
## Contribution guidelines ##

If you would like to contribute code you can do so through GitHub by forking the repository and sending a pull request.

***
## Who do I talk to? ##

* Alex Deaconu: alexdeaconu87@gmail.com