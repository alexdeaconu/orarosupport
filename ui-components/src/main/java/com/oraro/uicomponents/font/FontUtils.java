package com.oraro.uicomponents.font;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Helper class which can be used to set custom fonts to views
 * Created by alexdeaconu on 09/08/16.
 */
public final class FontUtils {

    private static final int DEFAULT_DEPTH = -1;

    private static FontUtils instance;

    private Uri mFontUri;
    private int mDepth = DEFAULT_DEPTH;
    private boolean mIsRecursive;
    private Map<View, Uri> mViewMap;

    private FontUtils() {
        // because order matters...
        mViewMap = new LinkedHashMap<>();
    }

    /**
     * Loads a font based on an {@link Uri}. Under the hood will call the
     * {@link Typeface#createFromFile(String)}. In order to set the font to the view
     * {@link FontUtils#commit()} must be called
     *
     * @param fontUri the uri to the font
     * @return the current {@link FontUtils} instance
     */
    public static FontUtils loadFontFromFile(Uri fontUri) {
        if (fontUri == null) {
            throw new IllegalArgumentException("Uri can't be null");
        }
        return getInstance(fontUri);
    }

    /**
     * Loads a font from the assets folder based on a relative path. Under the hood will call the
     * {@link Typeface#createFromAsset(AssetManager, String)}
     * In order to set the font to the view {@link FontUtils#commit()} must be called
     *
     * @param relativePath the relative path of the file related to the /assets folder.<br/>
     *                     I.e. if the font file is located under '/assets/fonts/custom-font.ttf',
     *                     the relativePath should be 'fonts/custom-font.ttf'
     * @return the current {@link FontUtils} instance
     */
    public static FontUtils loadFontFromAssets(String relativePath) {
        if (relativePath == null) {
            throw new IllegalArgumentException("Relative path can't be null");
        }
        Uri uri = Uri.parse(FontCache.ASSETS_PREFIX + relativePath);
        return getInstance(uri);
    }

    private static FontUtils getInstance(Uri fontUri) {
        if (instance == null) {
            synchronized (FontUtils.class) {
                if (instance == null) {
                    instance = new FontUtils();
                }
            }
        }
        instance.mFontUri = fontUri;
        return instance;
    }

    /**
     * @param views an array containing the views on which the font will be set
     * @return the current {@link FontUtils} instance
     */
    public FontUtils into(View... views) {
        if (views == null) {
            throw new IllegalArgumentException("View array is null");
        }
        Map.Entry<View, Uri> key;
        Iterator<Map.Entry<View, Uri>> it;

        for (View newView : views) {
            it = mViewMap.entrySet().iterator();
            while (it.hasNext()) {
                key = it.next();
                if (key.getKey().equals(newView)) {
                    it.remove();
                }
            }
            mViewMap.put(newView, mFontUri);
        }
        return this;
    }

    /**
     * In case the view is a {@link ViewGroup}, will go through it's children and set the font.
     * For the {@link TextView} instances, it will have no effect.
     *
     * @param depth i.e. for view=RelativeLayout, depth=2:
     *              <ul>
     *              <li>Iterates over RelativeLayout's children and sets the font for all the {@link TextView} instances (depth=1)</li>
     *              <li>Iterates over any {@link ViewGroup} child and tries setting the font for their children (depth=2)</li>
     *              </ul>
     * @return the current {@link FontUtils} instance
     * @throws IllegalArgumentException if depth is negative
     */
    public FontUtils depth(int depth) {
        if (depth < 0) {
            throw new IllegalArgumentException("Depth can't be <0");
        }
        mDepth = depth;
        return this;
    }

    /**
     * Sets the 'recursive' flag. If set to 'true', once {@link FontUtils#commit()} is called,
     * it will recursively iterate over each view's children and the same
     * font for each child. It will stop if it reaches the depth limit provided using
     * {@link FontUtils#depth(int)} or when it iterates over all its children.
     *
     * @param isRecursive by default {@code false}. If the view is a {@link ViewGroup} it will
     *                    recursively iterate over all its children and set the view's font.
     *                    <br><b><i>If the view is a {@link TextView} instance if will have no effect</i></b></br>
     * @return the current {@link FontUtils} instance
     * @see #depth(int)
     */
    public FontUtils recursive(boolean isRecursive) {
        mIsRecursive = isRecursive;
        return this;
    }

    /**
     * Applies fonts to all the views added using {@link FontUtils#into(View...)}.
     * This a blocking method. To be called on the UI thread.
     *
     * @return the current {@link FontUtils} instance
     */
    public FontUtils commit() {
        if (mViewMap.isEmpty() || mDepth < -1) {
            return this;
        }
        Context context = null;
        Typeface font;
        Iterator<Map.Entry<View, Uri>> it = mViewMap.entrySet().iterator();
        Map.Entry<View, Uri> pair;

        // setting the view for each view in the map
        while (it.hasNext()) {
            pair = it.next();
            if (context == null) {
                context = pair.getKey().getContext();
            }
            font = FontCache.INSTANCE.getFont(context, pair.getValue());
            if (mIsRecursive) {
                setFont(pair.getKey(), font, 0, mDepth);
            } else if (pair.getKey() instanceof TextView) {
                ((TextView) pair.getKey()).setTypeface(font);
            }
        }

        // clearing the map
        clearMap();

        return this;
    }

    /**
     * Clears the resources & destroys the singleton
     */
    public void dispose() {
        mIsRecursive = false;
        clearMap();
        FontCache.INSTANCE.clear();
        instance = null;
    }

    private void clearMap() {
        if (mViewMap != null) {
            // clearing the map
            Iterator<Map.Entry<View, Uri>> it = mViewMap.entrySet().iterator();
            while (it.hasNext()) {
                it.next();
                it.remove();
            }
        }
    }

    private static void setFont(View view, Typeface font, int currentDepth, int maxDepth) {
        if (view instanceof ViewGroup) {
            int childNumber = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < childNumber; i++) {
                View child = ((ViewGroup) view).getChildAt(i);
                // check if it's a class inheriting TextView
                if (child instanceof TextView) {
                    ((TextView) child).setTypeface(font);
                } else if (child instanceof ViewGroup) {
                    // it's a linear layout go through its children
                    if (currentDepth < maxDepth || maxDepth == DEFAULT_DEPTH) {
                        setFont(child, font, ++currentDepth, maxDepth);
                    }
                }
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(font);
            }
        }
    }


}
