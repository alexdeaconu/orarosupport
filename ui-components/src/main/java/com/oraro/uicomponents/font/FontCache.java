package com.oraro.uicomponents.font;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.util.ArrayMap;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Loads and caches the fonts (the {@link Typeface} instances)
 * Created by alexdeaconu on 09/08/16.
 */
enum FontCache {

    INSTANCE;

    static final String ASSETS_PREFIX = "file://android_asset/";

    private static final String TAG = FontCache.class.getName();

    private Map<Uri, WeakReference<Typeface>> fontMap;

    FontCache() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fontMap = new ArrayMap<>();
        } else {
            fontMap = new HashMap<>();
        }
    }

    Typeface getFont(Context context, Uri fontUri) {
        if (fontUri == null || context == null) {
            return null;
        }
        Typeface font = null;
        try {
            WeakReference<Typeface> fontRef = fontMap.get(fontUri);
            // there is a font ref, check it's not null
            if (fontRef != null) {
                font = fontRef.get();
                if (font != null) {
                    // font already cached, returning it
                    return font;
                } else {
                    font = getFontLoader(fontUri).load(context, fontUri);
                }
            } else {
                // font uri not found in cache. loading...
                font = getFontLoader(fontUri).load(context, fontUri);
            }

            // caching the font
            if (font != null) {
                fontMap.put(fontUri, new WeakReference<>(font));
            }
        } catch (Exception e) {
            Log.w(TAG, "Error retrieving font", e);
        }

        return font;
    }

    void clear() {
        if (fontMap != null) {
            fontMap.clear();
        }
    }

    private FontLoader getFontLoader(Uri uri) {
        String path = uri.toString();
        if (path.startsWith(ASSETS_PREFIX)) {
            return new AssestFontLoader();
        } else {
            return new FileFontLoader();
        }
    }

    interface FontLoader {

        Typeface load(Context context, Uri uri);
    }

    static class AssestFontLoader implements FontLoader {

        @Override
        public Typeface load(Context context, Uri uri) {
            String path = uri.toString();
            if (path.contains(ASSETS_PREFIX)) {
                path = path.substring(ASSETS_PREFIX.length());
            }
            return Typeface.createFromAsset(context.getAssets(), path);
        }
    }

    static class FileFontLoader implements FontLoader {

        @Override
        public Typeface load(Context context, Uri uri) {
            return Typeface.createFromFile(uri.toString());
        }
    }

}
