package com.oraro.uicomponents.view.list;

import android.content.Context;

import java.util.List;

/**
 * Custom adapter which can support multiple layouts, based on the list of
 * {@link SelectableItem} objects received.
 * <p/>
 * Created by alexandru.deaconu on 11/6/13.
 */
public class GenericListAdapter<T> extends OraroListAdapter<T> {

    /**
     * @param context used for accessing the layout inflater
     */
    public GenericListAdapter(Context context) {
        super(context);
    }

    public GenericListAdapter(Context context, List<T> itemList) {
        super(context, itemList);
    }
}
