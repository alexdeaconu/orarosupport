package com.oraro.uicomponents.view.list.annotation;

/**
 * Created by alexdeaconu on 21/08/16.
 */
public @interface ViewClickListener {

    int id() default DEFAULT_ID;

    /**
     * The id of the view to which to attach the {@link android.view.View.OnClickListener}
     */
    int DEFAULT_ID = -200;
}
