package com.oraro.uicomponents.view.list;

import android.util.SparseArray;
import android.view.View;

/**
 * <p>
 * Helper component used to implement the View Holder pattern when defining adapters for ui widgets like lists,
 * spinners, gallery views, etc.
 * </p>
 * <p/>
 * Created by alexandru.deaconu on 1/31/14.
 */
public final class ViewHolder {

    @SuppressWarnings("unchecked")
    public static <T extends View> T get(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            view.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }
        return (T) childView;
    }
}
