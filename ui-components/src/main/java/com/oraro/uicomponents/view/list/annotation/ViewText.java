package com.oraro.uicomponents.view.list.annotation;

/**
 * Created by alexdeaconu on 21/08/16.
 */
public @interface ViewText {

    int DEFAULT_ID = -200;

    /**
     * The {@link ViewText} (or any of its inherited classes), for which to set the text
     */
    int id() default DEFAULT_ID;

}
