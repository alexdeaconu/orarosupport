package com.oraro.uicomponents.view.list;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Build;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.oraro.uicomponents.view.list.annotation.ViewClickListener;
import com.oraro.uicomponents.view.list.annotation.ViewLayout;
import com.oraro.uicomponents.view.list.annotation.ViewLongClickListener;
import com.oraro.uicomponents.view.list.annotation.ViewText;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Custom adapter which can support multiple layouts, based on the list of
 * {@link SelectableItem} objects received.
 * <p/>
 * Created by alexandru.deaconu on 11/6/13.
 */
class OraroListAdapter<T> extends ArrayAdapter<T> {

    private static final String TAG = OraroListAdapter.class.getName();

    private static final int ARRAY_MAP_THRESHOLD = 1000;
    private static final int NO_VIEW_TYPE = -1;

    protected LayoutInflater mInflater;
    protected int mCurrentPosition = ArrayAdapter.NO_SELECTION;
    protected List<T> mItemList;
    protected List<T> mOriginalList;
    protected Map<Class<?>, Integer> mItemTypes;
    protected SparseIntArray mPositionSelection;
    protected int mViewTypeCount = NO_VIEW_TYPE;
    protected ArrayFilter mFilter;

    private DataSetObserver mDataObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            calculateItemTypes();
        }

        @Override
        public void onInvalidated() {
            calculateItemTypes();
        }
    };

    // made them instance variables for performance boost...
    private T mItem;
    private boolean mIsSelectableItem;

    private static <T> void populateView(T item, View view) throws IllegalAccessException {
        if (item == null || view == null) {
            return;
        }

        Class<?> clazz = item.getClass();
        Field[] fields = clazz.getDeclaredFields();
        View child;

        ViewText vt;
        ViewClickListener vcl;
        ViewLongClickListener vlcl;

        View.OnClickListener onClickListener;
        View.OnLongClickListener onLongClickListener;

        for (Field field : fields) {
            field.setAccessible(true);

            if ((vt = field.getAnnotation(ViewText.class)) != null) {
                Object text = field.get(item);
                if (text != null) {
                    if (vt.id() == ViewText.DEFAULT_ID) {
                        if (view instanceof TextView) {
                            ((TextView) view).setText(String.valueOf(text));
                        }
                    } else {
                        child = ViewHolder.get(view, vt.id());
                        if (child != null && child instanceof TextView) {
                            ((TextView) child).setText(String.valueOf(text));
                        }
                    }
                }
                field.setAccessible(false);
                continue;
            }

            if ((vcl = field.getAnnotation(ViewClickListener.class)) != null) {
                onClickListener = (View.OnClickListener) field.get(item);
                if (onClickListener != null) {
                    if (vcl != null) {
                        if (vcl.id() == ViewClickListener.DEFAULT_ID) {
                            view.setOnClickListener(onClickListener);
                        } else {
                            child = ViewHolder.get(view, vt.id());
                            if (child != null) {
                                child.setOnClickListener(onClickListener);
                            }
                        }
                    }
                }
                field.setAccessible(false);
                continue;
            }

            if ((vlcl = field.getAnnotation(ViewLongClickListener.class)) != null) {
                onLongClickListener = (View.OnLongClickListener) field.get(item);
                if (onLongClickListener != null) {
                    if (vlcl != null) {
                        if (vlcl.id() == ViewLongClickListener.DEFAULT_ID) {
                            view.setOnLongClickListener(onLongClickListener);
                        } else {
                            child = ViewHolder.get(view, vt.id());
                            if (child != null) {
                                child.setOnLongClickListener(onLongClickListener);
                            }
                        }
                    }
                }
                field.setAccessible(false);
                continue;
            }

            field.setAccessible(false);
        }
    }

    private static <T> int getLayoutRes(T item) throws IllegalAccessException {
        if (item == null) {
            return -1;
        }
        Class<?> clazz = item.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ViewLayout.class)) {
                return (int) field.get(item);
            }
        }
        return -1;
    }

    /**
     * @param context  used for accessing the layout inflater
     * @param itemList the item list to be displayed
     */
    OraroListAdapter(Context context, List<T> itemList) {
        super(context, android.R.layout.simple_list_item_1);
        init(context, itemList);
    }

    /**
     * @param context used for accessing the layout inflater
     */
    OraroListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        init(context, null);
    }

    private void init(Context context, List<T> itemList) {
        mItemList = itemList;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPositionSelection = new SparseIntArray();
        if (mItemList.size() <= ARRAY_MAP_THRESHOLD && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // for better performance
            mItemTypes = new ArrayMap<>();
        } else {
            mItemTypes = new HashMap<>();
        }

        registerDataSetObserver(mDataObserver);
    }

    @Override
    public int getCount() {
        return mItemList != null ? mItemList.size() : 0;
    }

    @Override
    public T getItem(int position) {
        return mItemList != null ? mItemList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mItemList == null) {
            return null;
        }
        mItem = mItemList.get(position);
        mIsSelectableItem = mItem instanceof SelectableItem;

        try {
            if (convertView == null) {
                if (mIsSelectableItem) {
                    convertView = mInflater.inflate(((SelectableItem) mItem).getLayoutRes(getContext()), parent, false);
                } else {
                    convertView = mInflater.inflate(getLayoutRes(mItem), parent, false);
                }
            }

            if (mIsSelectableItem) {
                ((SelectableItem) mItem).populateView(convertView);
            } else {
                populateView(mItem, convertView);
            }
        } catch (IllegalAccessException | ClassCastException e) {
            Log.w(TAG, "Could generate view", e);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return mViewTypeCount == NO_VIEW_TYPE ? calculateItemTypes() : mViewTypeCount;
    }

    @Override
    public int getItemViewType(int position) {
        return mViewTypeCount == NO_VIEW_TYPE ? 0 : mPositionSelection.get(position);
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    /**
     * Sets the index of the current selected item.
     *
     * @param position the list index of the current selected item
     */
    public void setCurrentSelected(int position) {
        mCurrentPosition = position;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public void setList(List<T> itemList) {
        if (itemList == null) {
            return;
        }

        mItemList = itemList;
        notifyDataSetChanged();
    }


    private int calculateItemTypes() {
        if (mItemList == null) {
            return 0;
        }

        mPositionSelection.clear();
        mItemTypes.clear();

        int i = 0;
        int typeIdx = 0;
        Integer existingIdx;
        Class<?> c;

        for (T item : mItemList) {
            c = item.getClass();
            existingIdx = mItemTypes.get(c);
            if (existingIdx == null) {
                mItemTypes.put(c, typeIdx);
                existingIdx = typeIdx;
                typeIdx++;
            }
            mPositionSelection.put(i, existingIdx);
            i++;
        }

        mViewTypeCount = mItemTypes.size();

        return mItemTypes.size();
    }

    class ArrayFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalList == null) {
                synchronized (OraroListAdapter.class) {
                    mOriginalList = new ArrayList<>(mItemList);
                }
            }

            if (TextUtils.isEmpty(prefix)) {
                results.values = mOriginalList;
                results.count = mItemList.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();
                final ArrayList<T> list = new ArrayList<>(mOriginalList);
                final ArrayList<T> filteredList = new ArrayList<>();

                Pattern p = Pattern.compile("(\\s|.)*(\\b(" + prefixString + ")(\\B)*)(\\s|.)*");
                String valueText;
                Matcher m;

                for (T item : list) {
                    valueText = item.toString().toLowerCase();

                    m = p.matcher(valueText);
                    if (m.matches()) {
                        filteredList.add(item);
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mItemList = (List<T>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
