//package com.oraro.uicomponents.view.list;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by a on 8/14/2014.
// */
//public class GenericMapAdapter<T> extends UICListAdapter<T> {
//
//    private final LayoutInflater inflater;
//    private List<T> itemList;
//    private Map<Class<?>, Integer> itemTypes;
//    private Map<Integer, Integer> positionSelection;
//
//    public GenericMapAdapter(Context context, List<T> itemList) {
//        super(context, itemList);
//        this.itemList = itemList;
//        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    public List<T> getList() {
//        return itemList;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (itemList == null) {
//            return null;
//        }
//
//        View row = convertView;
//        final SelectableItem item = itemList.get(position);
//
//        if (convertView == null) {
//            row = inflater.inflate(item.getLayoutRes(getContext()), parent, false);
//        }
//
//        item.populateView(row);
//
//        return row;
//    }
//
//    public void setItemList(List<T> itemList) {
//        if (itemList == null) {
//            return;
//        }
//
//        this.itemList = itemList;
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public int getCount() {
//        return itemList == null ? 0 : itemList.size();
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return isMultipleLayout ? calculateItemTypes() : 1;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return isMultipleLayout ? positionSelection.get(position) : 0;
//    }
//
//    @Override
//    public T getItem(int position) {
//        if (itemList !=null) {
//            return  itemList.get(position);
//        }
//        return null;
//    }
//
//    private int calculateItemTypes() {
//        if (itemList == null) {
//            return 1;
//        }
//        positionSelection = new HashMap<Integer, Integer>();
//        itemTypes = new HashMap<Class<?>, Integer>();
//
//        int i = 0;
//        int typeIdx = 0;
//        Integer existingIdx;
//
//        for (T item : itemList) {
//            Class<?> c = item.getClass();
//            existingIdx = itemTypes.get(c);
//            if (existingIdx == null) {
//                itemTypes.put(c, typeIdx);
//                existingIdx = typeIdx;
//                typeIdx++;
//            }
//            positionSelection.put(i, existingIdx);
//            i++;
//        }
//
//        return itemTypes.size();
//    }
//}
