package com.oraro.uicomponents.view.list.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker interface telling the field's value will be considered as a R.layout.* resource
 * <p/>
 * Created by alexdeaconu on 17/08/16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ViewLayout {
}
