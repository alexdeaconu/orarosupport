package com.oraro.uicomponents.view.list;

import android.content.Context;
import android.view.View;

/**
 * Created by alexdeaconu on 8/15/2014.
 */
public interface SelectableItem {

    int NO_RES = -1;

    /**
     * This is the callback where the text and listeners can be set to views. It receives the
     * item's layout view as a parameter.
     * <p>In order to improve performance, avoid calling
     * {@code convertView.findViewById(R.id.view_id)}. Use instead {@code ViewHolder.get(convertView, R.id.view_id)}</p>
     *
     * @param convertView the item's layout. Will be inflated with the value provided in
     *                    {@link #getLayoutRes(Context)}
     * @see SelectableItem#getLayoutRes(Context)
     */
    void populateView(View convertView);

    /**
     * The layout that will be inflated by the adapter when the item is displayed in the list
     *
     * @param context
     * @return
     */
    int getLayoutRes(Context context);
}
